# Workloads

## Deployments

    * Deployment is a good fit for managing a stateless application workload on your cluster
    * Any Pod in the Deployment is interchangeable and can be replaced if needed

## StatefulSet

    * lets you run one or more related Pods that do track state somehow
    * For example, if your workload records data persistently, you can run a StatefulSet that matches each Pod with a PersistentVolume
    *  Your code, running in the Pods for that StatefulSet, can replicate data to other Pods in the same StatefulSet to improve overall resilience

# Run a Stateless Application Using a Deployment
## Creating and exploring an nginx deployment

    * Create an nginx deployment.
    * Use kubectl to list information about the deployment.
    * Update the deployment.

    * You can run an application by creating a Kubernetes Deployment object, and you can describe a Deployment in a YAML file. 
    * For example, this YAML file describes a Deployment that runs the nginx:1.14.2 Docker image

    ```
    apiVersion: apps/v1
    kind: Deployment
    metadata:
    name: nginx-deployment
    spec:
    selector:
        matchLabels:
        app: nginx
    replicas: 2 # tells deployment to run 2 pods matching the template
    template:
        metadata:
        labels:
            app: nginx
        spec:
        containers:
        - name: nginx
            image: nginx:1.14.2
            ports:
            - containerPort: 80

    ```
            kubectl apply -f application/deployment.yml
            kubectl get deployments
            kubectl get pods -l app=nginx
            kubectl describe pod <pod-name>
            kubectl describe deployment nginx-deployment

## Updating the deployment

        ```
        apiVersion: apps/v1
        kind: Deployment
        metadata:
        name: nginx-deployment
        spec:
        selector:
            matchLabels:
            app: nginx
        replicas: 2
        template:
            metadata:
            labels:
                app: nginx
            spec:
            containers:
            - name: nginx
                image: nginx:1.16.1 # Update the version of nginx from 1.14.2 to 1.16.1
                ports:
                - containerPort: 80
        ```
        kubectl apply -f deployment-update.yml
        kubectl get pods -l app=nginx

## Scaling the application by increasing the replica count

    * You can increase the number of Pods in your Deployment by applying a new YAML file. 
    * This YAML file sets replicas to 4, which specifies that the Deployment should have four Pods

     ```
        apiVersion: apps/v1
        kind: Deployment
        metadata:
        name: nginx-deployment
        spec:
        selector:
            matchLabels:
            app: nginx
        replicas: 4
        template:
            metadata:
            labels:
                app: nginx
            spec:
            containers:
            - name: nginx
                image: nginx:1.16.1 # Update the version of nginx from 1.14.2 to 1.16.1
                ports:
                - containerPort: 80
        ```
        kubectl apply -f deployment-update.yml
        kubectl get pods -l app=nginx

## Deleting a deployment
