# Update apt repo
        sudo apt update

# Install prerequisite packages
        sudo apt install software-properties-common

# Add ansible repo
        sudo apt-add-repository --yes --update ppa:ansible/ansible
        sudo apt update

# Install Ansible
        sudo apt install ansible

